#add the following records to the blog_database #users

insert into users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

insert into users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");

insert into users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");

insert into users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

insert into users (email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

#add the following records to the blog_database #posts

insert into posts (user_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 01:00:00");

insert into posts (user_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");

insert into posts (user_id, title, content, datetime_posted) VALUES (2, "Third Code", "Welcome to mars!", "2021-01-02 03:00:00");

insert into posts (user_id, title, content, datetime_posted) VALUES (1, "Fourth Code", "bye bye solar system!", "2021-01-02 04:00:00");

#get all the post with an author ID of 1

select * from post where user_id = 1;		

#get all the users email and datetime of creation

SELECT email, datetime_created FROM users;

#update a post's content to "Hello to the people of the earth!"
#initial content is "Hello Earth!" by using the records ID

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

#delete the user with an email of "johndoe@gmail.com"	

DELETE FROM users WHERE id = 4;
